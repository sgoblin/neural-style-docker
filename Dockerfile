FROM kaixhin/torch:latest
MAINTAINER sgoblin "ryanm@redcow.club"

RUN apt-get install -y wget libprotobuf-dev protobuf-compiler && luarocks install loadcaffe
RUN git clone https://github.com/jcjohnson/neural-style.git
WORKDIR /root/torch/neural-style
RUN sh models/download_models.sh

